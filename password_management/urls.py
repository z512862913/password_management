# -*- coding: utf-8 -*-

from django.conf.urls import include, url
from django.contrib import admin
import main.views as views

urlpatterns = [
    # Examples:
    # url(r'^$', 'password_management.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^doc/', include('doc.urls')),
    url(r'^auth/login/$', views.login),
    url(r'^auth/logout/$', views.logout),
    url(r'^auth/register/$', views.new_user),
    url(r'^auth/change_password/$', views.change_user_password),
    url(r'^auth/forget_password/$', views.forget_password),
    url(r'^data/websites/$', views.get_all_support_website),
    url(r'^data/categories/$', views.get_all_category),
    url(r'^user/password_strength/$', views.check_password_strength),
    url(r'^user/random_strong_password/$', views.random_strong_password),
    url(r'^user/operate_account/$', views.operate_account),
    url(r'^user/get_usual_email/$', views.get_usual_email),
    url(r'^user/get_account_detail/$', views.get_account_detail),
    url(r'^user/get_all_my_account/$', views.get_all_my_account),
    url(r'^user/get_all_overdue_account/$', views.get_all_overdue_account),
    url(r'^user/clear_data/$', views.clear_all_data),
    url(r'^$', views.index),
]

# -*- coding: utf-8 -*-
import re
import random
import datetime
from Crypto import Random
from Crypto.Cipher import AES
from binascii import b2a_hex, a2b_hex


from django.contrib.auth.models import User
from django.db import models

import password_management.settings as settings

# Create your models here.


class Crypto:
    def __init__(self, key, initial_vector):
        # 这里密钥key 长度必须为16（AES-128）
        self.key = key[0:AES.key_size[0]]
        self.mode = AES.MODE_CBC
        self.initial_vector = initial_vector[0:AES.block_size]
        self.crypto = AES.new(self.key, self.mode, self.key)

    def encrypt(self, text):
        # 加密的文字必须是AES.block_size的倍数，不足的部分用空字符补齐
        add = AES.block_size - (len(text) % AES.block_size)
        text += ('\0' * add)
        # 这里统一把加密后的字符串转化为16进制字符串，便于debug
        return b2a_hex(self.crypto.encrypt(text))
        # return self.crypto.encrypt(text)

    # 解密后，去掉补足的空字符用strip() 去掉
    def decrypt(self, text):
        plain_text = self.crypto.decrypt(a2b_hex(text))
        # plain_text = self.crypto.decrypt(text)
        return plain_text.rstrip('\0')


class Website(models.Model):
    name = models.CharField(verbose_name=u'网站名称', max_length=100)
    login_url = models.CharField(verbose_name=u'网站登录url', max_length=100)
    icon_path = models.CharField(verbose_name=u'图标路径', max_length=100)

    def get_detail(self):
        """
        获取此网站的详情
        :return: dic
        """
        data = {
            'id': self.id,
            'name': self.name,
            'login_url': self.name,
            'icon_path': self.icon_path,
        }
        return data


class AccountCategory(models.Model):
    name = models.CharField(verbose_name=u'分类名称', max_length=100)
    icon_path = models.CharField(verbose_name=u'图标路径', max_length=100)

    def get_detail(self):
        """
        获取此帐号类别的详情
        :return:
        """
        data = {
            'id': self.id,
            'name': self.name,
            'icon_path': self.icon_path,
        }
        return data


def is_email(username):
    if re.search(r'[\w\s]+@[\w\s]+.\w+', username):
        return True
    else:
        return False


class MyUser(models.Model):
    user = models.OneToOneField(verbose_name=u'对应用户', to=User)
    usual_email = models.CharField(verbose_name=u'常用账户', default='', max_length=100)

    @staticmethod
    def forget_password(main_username, username1, password1, username2, password2, username3, password3, new_password):
        """
        通过输入3个已经储存的账户密码来重置密码
        :param main_username:
        :param username1:
        :param password1:
        :param username2:
        :param password2:
        :param username3:
        :param password3:
        :param new_password:
        :return:
        """
        account_id_list = []
        if MyUser.objects.filter(user__username=main_username):
            target_user = MyUser.objects.get(user__username=main_username)
            account1_correct = False
            for account in target_user.account_set.filter(username=username1):
                if account.get_password() == password1:
                    account_id_list.append(account.id)
                    account1_correct = True
                    break
            account2_correct = False
            for account in target_user.account_set.filter(username=username2):
                if account.get_password() == password2 and account.id not in account_id_list:
                    account_id_list.append(account.id)
                    account2_correct = True
                    break
            account3_correct = False
            for account in target_user.account_set.filter(username=username3):
                if account.get_password() == password3 and account.id not in account_id_list:
                    account_id_list.append(account.id)
                    account3_correct = True
                    break
            if account1_correct and account2_correct and account3_correct:
                target_user.change_my_user_password(new_password)
                return True
            else:
                return False
        else:
            return False

    def new_account(self, website_id, username, password, category_id):
        """
        为用户存入一个新的密码
        :param website_id:
        :param username:
        :param password:
        :param category_id:
        :return:
        """
        try:
            new_account = Account(owner=self, website_id=website_id, username=username, category_id=category_id)
        except Website.DoesNotExist as error:
            return str(error), False
        except AccountCategory.DoesNotExist as error:
            return str(error), False
        new_account.password_strength = Account.check_password_strength(password=password)
        new_account.save()
        new_account.change_password(password)
        if is_email(username=username):
            self.calculate_usual_email()
        return new_account.get_detail(), True

    def delete_account(self, account_id):
        if self.account_set.filter(id=account_id):
            self.account_set.get(id=account_id).delete()
            return 0, True
        else:
            return 3, False

    def calculate_usual_email(self):
        """
        计算用户最常用的账户名
        :return: string
        """
        email_count = {}
        top_email = ''
        top_count = 0
        for account in self.account_set.all():
            if is_email(username=account.username):
                if account.username not in email_count:
                    email_count[account.username] = 1
                else:
                    email_count[account.username] += 1
                if email_count[account.username] > top_count:
                    top_count = email_count[account.username]
                    top_email = account.username
        self.usual_email = top_email
        self.save()
        return top_email

    def change_my_user_password(self, password):
        """
        为用户重置密码
        :param password: 新密码
        :return:
        """
        self.user.set_password(raw_password=password)
        self.user.save()
        return 0, True

    def get_all_account(self):
        """
        获取用户所有储存的账户密码
        :return: list[{category.name: list[id]}]
        """
        data = []
        for category in AccountCategory.objects.all():
            category_dic = {
                'name': category.name,
                'accounts': [],
            }
            for account in Account.objects.filter(owner=self, category_id=category.id):
                category_dic['accounts'].append(account.get_detail())
            data.append(category_dic)
        return data

    def delete_all_data(self):
        """
        为用户清空所有储存的数据
        :return:
        """
        for account in self.account_set.all():
            account.delete()
        self.usual_email = ''
        self.save()
        return 0, True

    def get_usual_email(self):
        """
        获取最常用的账户名
        :return:
        """
        return self.usual_email

    def get_all_overdue_account(self):
        """
        获取所有密码过于陈旧建议修改密码的账户id
        :return: list
        """
        account_id_list = []
        for account in self.account_set.all():
            if account.check_time_delta():
                account_id_list.append(account.id)
        return account_id_list


class Account(models.Model):
    PASSWORD_STRENGTH_CHOICE = {
        (u'强', u'强'),
        (u'中', u'中'),
        (u'弱', u'弱'),
    }
    owner = models.ForeignKey(verbose_name=u'所属用户', to=MyUser)
    website = models.ForeignKey(verbose_name=u'帐号所在的网站', to=Website)
    username = models.CharField(verbose_name=u'用户名', max_length=100)
    password = models.CharField(verbose_name=u'加密后的密码', max_length=100, default='')
    password_strength = models.CharField(verbose_name=u'密码强度', max_length=10, choices=PASSWORD_STRENGTH_CHOICE)
    change_password_time = models.DateField(verbose_name=u'修改密码时间', auto_now_add=True)
    category = models.ForeignKey(verbose_name=u'所属分类', to=AccountCategory)
    # 这里把iv写入数据库对安全性没有影响
    # The IV can be made public, but it must be authenticated by the receiver and it should be picked randomly.
    # 摘自NIST SP800-38A, Section 6.2.
    encrypt_initial_vector = models.TextField(verbose_name=u'加密所用的iv', default='')

    def get_crypto(self):
        """
        获取这个账户专用的加密解密器
        :return: Crypto instance
        """
        # 使用修改密码的时间作为key的一个生成变量，可以使得每次修改密码的key都不一样，安全性有保证
        # 加入各种符号和字母是为了组成强密码，防爆破
        return Crypto(key=self.change_password_time.strftime('!@%YAX%m&*%d$^'),
                      initial_vector=self.get_iv())

    def get_detail(self):
        """
        获取这个账户的详情
        :return: dic
        """
        data = {
            'id': self.id,
            'website': self.website.get_detail(),
            'username': self.username,
            'password': self.get_password(),
            'password_strength': self.password_strength,
            'change_password_time': self.change_password_time.strftime('%Y年%m月%d日'),
            'category': self.category.get_detail(),
            'is_overdue': self.check_time_delta()
        }
        return data

    def get_password(self):
        """
        将数据库里存的加密的密码转化为明文
        :return:
        """
        crypto = self.get_crypto()
        raw_password = crypto.decrypt(text=self.password)
        return raw_password

    def __save_new_iv(self):
        """
        内置方法，生成随机的 initial vector 用于AES加密
        并转化为ASCII存入数据库
        :return:
        """
        # 根据 NIST SP800-38A, Section 6.2 随机选取iv
        self.encrypt_initial_vector = b2a_hex(Random.new().read(AES.block_size))
        self.save()

    def get_iv(self):
        """
        将数据库中存的以ASCII字符表示的 initial vector 转化为原始的值
        :return:
        """
        return a2b_hex(self.encrypt_initial_vector)

    def change_password(self, password):
        """
        为用户存入的账户修改密码
        :param password:
        :return:
        """
        # 确定密码的强度
        self.password_strength = self.check_password_strength(password=password)
        # 加密密码
        # 根据 NIST SP800-38A, Section 6.2 随机选取iv
        self.__save_new_iv()
        crypto = self.get_crypto()
        encrypted_password = crypto.encrypt(text=password)
        self.password = unicode(encrypted_password)
        self.save()
        return 0, True

    def check_time_delta(self):
        """
        检查此账户的密码是否应该建议修改
        :return: is_overdue
        """
        time_delta = (datetime.date.today() - self.change_password_time)
        if time_delta <= settings.RECOMMENDED_CHANGE_PASSWORD_TIME_DELTA:
            return False
        else:
            return True

    @staticmethod
    def check_password_strength(password):
        """
        检查密码强度
        :param password:
        :return: string 强/中/弱
        """
        if re.search(r'[A-Z]+', password) and re.search(r'[a-z]+', password):
            if re.search(r'[^\w\s]+', password):
                return u'强'
            else:
                return u'中'
        else:
            return u'弱'

    @staticmethod
    def random_strong_password(allow_uppercase, allow_underscore, allow_special_characters):
        """
        为用户随机生成强密码
        :param allow_uppercase:
        :param allow_underscore:
        :param allow_special_characters:
        :return:
        """
        source_string = 'abcdefghijklmnopqrstuvwxyz0123456789'
        strong_password = 'z'
        if allow_uppercase:
            source_string += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        if allow_underscore:
            source_string += '_'
            strong_password += '_'
        if allow_special_characters:
            source_string += '!@#$%^&*()+-='
        for i in range(0, 10):
            strong_password += source_string[random.randint(0, len(source_string)-1)]
        return strong_password


# -*- coding: utf-8 -*-
import json

from functools import wraps

from django.http import JsonResponse

from django.shortcuts import render_to_response
from django.views.decorators.csrf import ensure_csrf_cookie

from django.contrib.auth import authenticate
from django.contrib.auth import login as sys_login, logout as sys_logout
from django.contrib.auth.models import User

from models import Website, Account, AccountCategory, MyUser

# Create your views here.


def customized_login_required(method):
    """
    自动检测登陆状态并执行错误返回
    :param method:
    :return:
    """
    @wraps(method)
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated():
            if request.user.is_staff:
                sys_logout(request)
                # 如果是超级用户就登出然后返回未登录状态
                status = {'code': 2, 'message': '用户未登录'}
                result = {'_status': status}
                return JsonResponse(result, status=401)
            else:
                return method(request, *args, **kwargs)
        else:
            # 用户未登陆
            status = {'code': 2, 'message': '用户未登录'}
            result = {'_status': status}
            return JsonResponse(result, status=401)
    return wrapper


@ensure_csrf_cookie
def index(request):
    return render_to_response('index.html')


def logout(request):
    sys_logout(request)
    status = {'code': 0, 'message': '用户注销成功'}
    result = {'_status': status}
    return JsonResponse(result, status=200)


def login(request):
    status = {'code': 0, 'message': 'unknown'}
    result = {'_status': status}
    if request.method == 'POST':
        if request.user.is_authenticated():
            logout(request)
        request_data = json.loads(request.body)
        username = request_data.get('username')
        password = request_data.get('password')
        user = authenticate(username=username, password=password)
        if user:
            sys_login(request, user)
            status['message'] = '用户登录成功'
            return JsonResponse(result, status=200)
        else:
            status['code'] = 1
            status['message'] = '用户名或密码错误'
            return JsonResponse(result, status=403)
    elif request.method == 'GET':
        status['message'] = '成功获取用户登陆状态'
        if request.user.is_authenticated():
            if request.user.is_staff:
                sys_logout(request)
                result['has_logged_in'] = False
                return JsonResponse(result, status=200)
            else:
                result['has_logged_in'] = True
                result['id'] = str(request.user.myuser.id)
                return JsonResponse(result, status=200)
        else:
            result['has_logged_in'] = False
            return JsonResponse(result, status=200)
    else:
        status['code'] = -1
        status['message'] = '请求方式错误'
        return JsonResponse(result, status=405)


def new_user(request):
    status = {'code': 0, 'message': 'unknown'}
    result = {'_status': status}
    if request.method == 'POST':
        request_data = json.loads(request.body)
        username = request_data.get('username')
        password = request_data.get('password')
        if User.objects.filter(username=username):
            status['code'] = 4
            status['message'] = '该用户名已被注册'
            return JsonResponse(result, status=400)
        else:
            user = User.objects.create_user(username=username, password=password)
            new_my_user = MyUser(user=user)
            new_my_user.save()
            result['id'] = new_my_user.id
            status['message'] = '新用户注册成功'
            return JsonResponse(result, status=200)
    else:
        status['code'] = -1
        status['message'] = '请求方式错误'
        return JsonResponse(result, status=405)


@customized_login_required
def change_user_password(request):
    status = {'code': 0, 'message': 'unknown'}
    result = {'_status': status}
    user = request.user
    if request.method == 'PUT':
        request_data = json.loads(request.body)
        old_password = request_data.get('old_password')
        new_password = request_data.get('new_password')
        if authenticate(username=user.username, password=old_password):
            user.set_password(new_password)
            user.save()
            status['message'] = '用户修改密码成功'
            return JsonResponse(result, status=200)
        else:
            status['code'] = 1
            status['message'] = '原密码错误'
            return JsonResponse(result, status=403)
    else:
        status['code'] = -1
        status['message'] = '请求方式错误'
        return JsonResponse(result, status=405)


def forget_password(request):
    status = {'code': 0, 'message': 'unknown'}
    result = {'_status': status}
    if request.method == 'PUT':
        request_data = json.loads(request.body)
        main_username = request_data.get('main_username')
        username1 = request_data.get('username1')
        password1 = request_data.get('password1')
        username2 = request_data.get('username2')
        password2 = request_data.get('password2')
        username3 = request_data.get('username3')
        password3 = request_data.get('password3')
        new_password = request_data.get('new_password')
        success = MyUser.forget_password(main_username=main_username,
                                         username1=username1, password1=password1,
                                         username2=username2, password2=password2,
                                         username3=username3, password3=password3,
                                         new_password=new_password)
        if success:
            status['message'] = '重置密码成功，欢迎回来'
            return JsonResponse(result, status=200)
        else:
            status['code'] = 4
            status['message'] = '认证失败，未能重置密码'
            return JsonResponse(result, status=403)


@customized_login_required
def clear_all_data(request):
    status = {'code': 0, 'message': 'unknown'}
    result = {'_status': status}
    user = request.user.myuser
    if request.method == 'DELETE':
        user.delete_all_data()
        status['message'] = '用户全部数据清除成功'
        return JsonResponse(result, status=200)
    else:
        status['code'] = -1
        status['message'] = '请求方式错误'
        return JsonResponse(result, status=405)


def get_all_support_website(request):
    status = {'code': 0, 'message': 'unknown'}
    result = {'_status': status}
    if request.method == 'GET':
        website_data = []
        for website in Website.objects.all():
            website_data.append(website.get_detail())
        result['websites'] = website_data
        status['message'] = '获取所有支持的网站成功'
        return JsonResponse(result, status=200)
    else:
        status['code'] = -1
        status['message'] = '请求方式错误'
        return JsonResponse(result, status=405)


def get_all_category(request):
    status = {'code': 0, 'message': 'unknown'}
    result = {'_status': status}
    if request.method == 'GET':
        category_data = []
        for category in AccountCategory.objects.all():
            category_data.append(category.get_detail())
        result['categories'] = category_data
        status['message'] = '获取所有账户分类成功'
        return JsonResponse(result, status=200)
    else:
        status['code'] = -1
        status['message'] = '请求方式错误'
        return JsonResponse(result, status=405)


def check_password_strength(request):
    status = {'code': 0, 'message': 'unknown'}
    result = {'_status': status}
    if request.method == 'PUT':
        request_data = json.loads(request.body)
        password = request_data.get('password')
        strength = Account.check_password_strength(password=password)
        result['strength'] = strength
        status['message'] = '获取此密码强度成功'
        return JsonResponse(result, status=200)
    else:
        status['code'] = -1
        status['message'] = '请求方式错误'
        return JsonResponse(result, status=405)


def random_strong_password(request):
    status = {'code': 0, 'message': 'unknown'}
    result = {'_status': status}
    if request.method == 'GET':
        allow_uppercase = request.GET.get('allow_uppercase') == "true"
        allow_underscore = request.GET.get('allow_underscore') == "true"
        allow_special_character = request.GET.get('allow_special_character') == "true"
        result['password'] = Account.random_strong_password(allow_uppercase=allow_uppercase,
                                                            allow_underscore=allow_underscore,
                                                            allow_special_characters=allow_special_character)
        status['message'] = '获取随机强密码成功'
        return JsonResponse(result, status=200)
    else:
        status['code'] = -1
        status['message'] = '请求方式错误'
        return JsonResponse(result, status=405)


@customized_login_required
def operate_account(request):
    status = {'code': 0, 'message': 'unknown'}
    result = {'_status': status}
    user = request.user.myuser
    if request.method == 'POST':
        request_data = json.loads(request.body)
        website_id = request_data.get('website_id')
        username = request_data.get('username')
        password = request_data.get('password')
        category_id = request_data.get('category_id')
        if website_id and username and password and category_id:
            detail, success = user.new_account(website_id=website_id, username=username, password=password,
                                               category_id=category_id)
            if success:
                status['message'] = '账户储存成功，我们会好好保管哒'
                result['account'] = detail
                return JsonResponse(result, status=200)
            else:
                status['code'] = 555
                status['message'] = detail
                return JsonResponse(result, status=400)
    if request.method == 'PUT':
        request_data = json.loads(request.body)
        password = request_data.get('password')
        account_id = request_data.get('account_id')
        try:
            account = Account.objects.get(id=account_id)
        except Account.DoesNotExist:
            status['code'] = 3
            status['message'] = '不存在此账户'
            return JsonResponse(result, status=403)
        if account.owner == user:
            account.change_password(password=password)
            status['message'] = '修改账户的密码成功'
        else:
            status['code'] = 2
            status['message'] = '无法尝试操作的账户'
            return JsonResponse(result, status=403)
        return JsonResponse(result, status=200)
    if request.method == 'DELETE':
        request_data = json.loads(request.body)
        account_id = request_data.get('account_id')
        error_code, success = user.delete_account(account_id=account_id)
        if success:
            status['message'] = '删除账户成功'
            return JsonResponse(result, status=200)
        else:
            status['code'] = error_code
            status['message'] = '无法尝试操作的账户'
            return JsonResponse(result, status=400)
    else:
        status['code'] = -1
        status['message'] = '请求方式错误'
        return JsonResponse(result, status=405)


@customized_login_required
def get_usual_email(request):
    status = {'code': 0, 'message': 'unknown'}
    result = {'_status': status}
    user = request.user.myuser
    if request.method == 'GET':
        usual_email = user.get_usual_email()
        if usual_email:
            result['email'] = usual_email
            status['message'] = '获取常用email成功'
            return JsonResponse(result, status=200)
        else:
            result['email'] = ''
            status['message'] = '用户暂时无常用email'
            return JsonResponse(result, status=200)
    else:
        status['code'] = -1
        status['message'] = '请求方式错误'
        return JsonResponse(result, status=405)


@customized_login_required
def get_account_detail(request):
    status = {'code': 0, 'message': 'unknown'}
    result = {'_status': status}
    user = request.user.myuser
    if request.method == 'GET':
        account_id = request.GET.get('account_id')
        if user.account_set.filter(id=account_id):
            account = Account.objects.get(id=account_id)
            result['account'] = account.get_detail()
            status['message'] = '获取账户详情成功'
            return JsonResponse(result, status=200)
        else:
            status['code'] = 4
            status['message'] = '你没有这样的账户'
            return JsonResponse(result, status=403)
    else:
        status['code'] = -1
        status['message'] = '请求方式错误'
        return JsonResponse(result, status=405)


@customized_login_required
def get_all_my_account(request):
    status = {'code': 0, 'message': 'unknown'}
    result = {'_status': status}
    user = request.user.myuser
    if request.method == 'GET':
        result['accounts'] = user.get_all_account()
        status['message'] = '获取用户所有账户成功'
        return JsonResponse(result, status=200)
    else:
        status['code'] = -1
        status['message'] = '请求方式错误'
        return JsonResponse(result, status=405)


@customized_login_required
def get_all_overdue_account(request):
    status = {'code': 0, 'message': 'unknown'}
    result = {'_status': status}
    user = request.user.myuser
    if request.method == 'GET':
        result['accounts'] = user.get_all_overdue_account()
        status['message'] = '获取用户所有密码陈旧建议修改的账户成功'
        return JsonResponse(result, status=200)
    else:
        status['code'] = -1
        status['message'] = '请求方式错误'
        return JsonResponse(result, status=405)
import api from "api";

import Promise from "promise";

// export function logger({ dispatch }, promise) {
//     if (promise && promise.catch) {
//         return promise.catch(
//             error => {
//                 let contents = [ message.error.unknown ];
//                 if (error && error. status) {
//                     contents = [ error. status.message ];
//                 }

//                 dispatch(mutations.SHOW POPUP, {
//                     alert: true,
//                     duration: 2000,
//                     contents,
//                 });
//                 return Promise.reject(error);
//             }
//         );
//     }
//     return promise;
// }

export function getLoginStatus({ dispatch, state }) {
    if (typeof state.hasLoggedIn !== "boolean") {
        return api.getLoginStatus()
            .then(({ id, hasLoggedIn }) => {
                dispatch("update login status", hasLoggedIn);
                dispatch("update current account", { id });
            })
            .catch(() => dispatch("update login status", false));
    }
    return Promise.resolve();
}

export function logIn({ dispatch, state }, account) {
    return api.logIn(account)
        .then(() => {
            dispatch("update login status", true);
            dispatch("update current account", account);
        });
}

export function logOut({ dispatch }) {
    return api.logOut()
        .then(() => {
            dispatch("update login status", false);
            window.location.reload();
        });
}

export function register({}, account) {
    return api.register(account);
}

export function changePassword({ dispatch }, form) {
    return api.changePassword(form)
        .then(() => dispatch("update login status", false));
}

export function resetPassword({}, form) {
    return api.resetPassword(form);
}

export function getWebsites({ dispatch, state }) {
    if (!state.websites) {
        return api.getWebsites()
            .then(({ websites }) => dispatch("receive websites", websites));
    }
    return Promise.resolve();
}

export function getCategories({ dispatch, state }) {
    if (!state.categories) {
        return api.getCategories()
            .then(({ categories }) => dispatch("receive categories", categories));
    }
    return Promise.resolve();
}

export function getPasswordStrength({}, form) {
    return api.getPasswordStrength(form);
}

export function getRandomPassword({}, form) {
    return api.getRandomPassword(form);
}

export function getAccountList({ dispatch, state }) {
    if (!state.accounts) {
        return api.getAccountList()
            .then(({ accounts = [] }) => dispatch("receive account list", accounts));
    }
    return Promise.resolve();
}

export function saveAccountItem({ dispatch }, account) {
    return api.saveAccountItem(account)
        .then(({ account }) => dispatch("save account item", account));
}

export function deleteAccountItem({ dispatch }, account) {
    const { id } = account;
    return api.deleteAccountItem({ accountId: id })
        .then(() => dispatch("delete account item", account));
}

export function updateAccountItem({ dispatch }, account, modification) {
    const { id } = account;
    return api.updateAccountItem(
            Object.assign({ accountId: id }, modification),
            account
        )
        .then(() => dispatch("update account item", account, modification));
}

export function hasLoggedIn(state) {
    return state.hasLoggedIn;
}

export function websites(state) {
    return (state.websites || []);
}

export function categories(state) {
    return (state.categories || []);
}

export function accounts(state) {
    return (state.accounts || []);
}

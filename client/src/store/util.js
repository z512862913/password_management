export function isSameAccount(x, y) {
    return (x.id === y.id);
}

export function hasAccount(list, _acccount) {
    return list.some(account => isSameAccount(account, _acccount));
}

export function findAccount(list, _acccount) {
    return list.find(account => isSameAccount(account, _acccount));
}

import Vue from "vue";
import Vuex from "vuex";

const state = {
    hasLoggedIn: null,
    currentAccount: null,
    accounts: null,
    categories: null,
    websites: null,
};

/* eslint-disable no-param-reassign */
const mutations = {
    ["update login status"](state, hasLoggedIn) {
        state.hasLoggedIn = hasLoggedIn;
    },

    ["update current account"](state, account) {
        state.currentAccount = account;
    },

    ["receive account list"](state, accounts) {
        state.accounts = accounts;
    },

    ["save account item"](state, account) {
        const { accounts } = state;
        const _name = account.category.name;
        if (accounts instanceof Array) {
            const item = accounts.find(({ name }) => (name === _name)) || {};
            const list = item.accounts || [];
            list.push(account);
        }
    },

    ["update account item"](state, account, modification) {
        const { accounts } = state;
        const _name = account.category.name;
        if (accounts instanceof Array) {
            const item = accounts.find(({ name }) => (name === _name)) || [];
            const list = item.accounts || [];
            for (let i = 0; i < list.length; i++) {
                if (list[i].id === account.id) {
                    list.$set(i, Object.assign({}, list[i], modification));
                    return;
                }
            }
        }
    },

    ["delete account item"](state, account) {
        const { accounts } = state;
        const _name = account.category.name;
        const _id = account.id;
        if (accounts instanceof Array) {
            const item = accounts.find(({ name }) => (name === _name)) || {};
            const list = item.accounts;
            item.accounts = list.filter(({ id }) => (id !== _id));
        }
    },

    ["receive websites"](state, websites) {
        state.websites = websites;
    },

    ["receive categories"](state, categories) {
        state.categories = categories;
    },
};
/* eslint-enable no-param-reassign */

// install plugin
Vue.use(Vuex);

export default new Vuex.Store({
    state,
    mutations,
});

import Vue from "vue";
import VueResource from "vue-resource";

// install plugin
Vue.use(VueResource);

import * as resource from "./resource";

import Promise from "promise";

function extractData(response) {
    if (response && response.data) {
        return response.data;
    }
    return response;
}

function transformProperties(obj, isUnderToCamel = true) {
    if (typeof obj !== "object") {
        return obj;
    }

    function underToCamel(str) {
        return str.replace(/_(.|$)/g, (str, chr) => chr.toUpperCase());
    }

    function camelToUnder(str) {
        return str.replace(/[A-Z]/g, chr => `_${chr.toLowerCase()}`);
    }

    if (obj instanceof Array) {
        return obj.map(item => transformProperties(item, isUnderToCamel));
    }

    const ret = {};
    for (const property in obj) {
        if (obj.hasOwnProperty(property)) {
            const _property = isUnderToCamel ? underToCamel(property) : camelToUnder(property);
            ret[_property] = transformProperties(obj[property], isUnderToCamel);
        }
    }

    return ret;
}

const api = {};
const keys = Object.keys(resource);
keys.forEach(key => (
    api[key] = (_data = {}) => {
        const { url, method } = resource[key];
        const data = transformProperties(_data, false);
        return Vue.http({ url, method, data })
            .then(
                data => Promise.resolve(
                    transformProperties(
                        extractData(data)
                    )
                )
            );
    }
));

export default api;

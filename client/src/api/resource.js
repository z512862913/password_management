export const getLoginStatus = {
    url: "/auth/login/",
    method: "GET",
};

export const logIn = {
    url: "/auth/login/",
    method: "POST",
};

export const logOut = {
    url: "/auth/logout/",
    method: "POST",
};

export const register = {
    url: "/auth/register/",
    method: "POST",
};

export const changePassword = {
    url: "/auth/change_password/",
    method: "PUT",
};

export const resetPassword = {
    url: "/auth/forget_password/",
    method: "PUT",
};

export const getWebsites = {
    url: "/data/websites/",
    method: "GET",
};

export const getCategories = {
    url: "/data/categories/",
    method: "GET",
};

export const getPasswordStrength = {
    url: "/user/password_strength/",
    method: "PUT",
};

export const getRandomPassword = {
    url: "/user/random_strong_password/",
    method: "GET",
};

export const getAccountList = {
    url: "/user/get_all_my_account/",
    method: "GET",
};

export const getAccountItem = {
    url: "/user/get_account_detail/",
    method: "GET",
};

export const saveAccountItem = {
    url: "/user/operate_account/",
    method: "POST",
};

export const deleteAccountItem = {
    url: "/user/operate_account/",
    method: "DELETE",
};

export const updateAccountItem = {
    url: "/user/operate_account/",
    method: "PUT",
};

export const getUsualEmail = {
    url: "/user/get_usual_email/",
    method: "GET",
};

export const getOverdueAccountList = {
    url: "/user/get_all_overdue_account/",
    method: "GET",
};

export const clearData = {
    url: "/user/clear_data",
    method: "DELETE",
};

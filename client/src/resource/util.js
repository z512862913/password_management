export function extractMessage(error, defValue) {
    let message;
    if (error && error._status) {
        message = error._status.message;
    }
    return (message || defValue);
}

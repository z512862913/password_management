import Vue from "vue";
import VueResource from "vue-resource";

import { message } from "config";
import Cookies from "js-cookie";

// install plugin
Vue.use(VueResource);

// global vue-resource configuration
Vue.http.options.headers = {
    "Cache-Control": "no-cache",
    Accept: "application/json",
};
Vue.http.options.beforeSend = (request) => {
    const ret = Object.assign({}, request);
    if (!/http(|s):.*/.test(request.url)) {
        ret.headers["X-CSRFToken"] = Cookies.get("csrftoken") || "";
    }
    return ret;
};

// push a vue-resource interceptor
Vue.http.interceptors.push({
    response(response) {
        const ret = Object.assign({}, response, {
            ok: false,
            _status: {
                message: message.error.api,
            },
        });
        if ((response.headers("content-type") === "application/json") &&
            response.data &&
            response.data._status) {
            const status = response.data._status;
            ret.ok = Number(status.code) === 0;
            ret._status = {
                code: Number(status.code),
                message: (
                    (typeof status.message === "string") &&
                    status.message ||
                    ret.message
                ),
            };
        }
        return ret;
    },
});

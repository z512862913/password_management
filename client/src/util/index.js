
export function getCurrentOperatingSystem() {
    const userAgent = window.navigator.userAgent;
    return {
        trident: userAgent.indexOf("Trident") > -1,
        presto: userAgent.indexOf("Presto") > -1,
        webKit: userAgent.indexOf("AppleWebKit") > -1,
        gecko: userAgent.indexOf("Gecko") > -1 && userAgent.indexOf("KHTML") === -1,
        mobile: !!userAgent.match(/AppleWebKit.*Mobile.*/),
        iOS: !!userAgent.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/),
        android: userAgent.indexOf("Android") > -1 || userAgent.indexOf("Linux") > -1,
        iPhone: userAgent.indexOf("iPhone") > -1,
        iPad: userAgent.indexOf("iPad") > -1,
        webApp: userAgent.indexOf("Safari") === -1,
    };
}

/* eslint-disable no-param-reassign */
/**
 * add class for dom element
 * @param  {Element} el
 * @param  {String | Array} rawClassList  string seperated by comma | class list array
 */
export function elAddClass(el, rawClassList) {
    const classList = (typeof rawClassList === "string") ? rawClassList.split(",") : rawClassList;
    if (el.classList) {
        el.classList.add(classList);
    }
    else {
        el.className += ((el.className.length > 0) ? " " : "") + classList.join(",");
    }
}
/* eslint-enable no-cond-assign */

/**
 * [elAfterMounted description]
 * @param  {[type]}    el   [description]
 * @param  {...[type]} args [description]
 * @return {[type]}         [description]
 */
export function elAfterMounted(el, ...args) {
    if (args.length === 0) {
        return;
    }

    let interval = 100;
    if (args.length === 2) {
        interval = args[0];
    }

    const cb = args.pop();
    if (!(cb instanceof Function)) {
        return;
    }

    if (!el || (el.toString().indexOf("Element") === -1) ||
            !!el.offsetParent) {
        cb();
    }

    const instance = setInterval(() => {
        window.el = el;
        if (!!el.offsetParent) {
            clearInterval(instance);
            cb();
        }
    }, interval);
}

/**
 * deep copy objects
 * @param  {Object}    target
 * @param  {[Object]}  args   source object list
 * @return {Object}    reference of target
 */
export function extend(target, ...args) {
    let ret = target;
    args.forEach((src) => {
        const obj = {};
        for (const key in src) {
            if (src.hasOwnProperty(key)) {
                if (src[key] instanceof Array) {
                    obj[key] = src[key].slice();
                }
                else if ((typeof src[key] === "object") && ((src[key] || null) !== null)) {
                    obj[key] = extend({}, src[key]);
                }
                else {
                    obj[key] = src[key];
                }
            }
        }
        ret = Object.assign(ret, obj);
    });
    return ret;
}

/**
 * Avoiding accessing null's property
 * when accessing object property value recursively
 * @param  {Object}    context
 * @param  {String}    keyString   like "prop1.prop2" means context.prop1.prop2
 * @return {Any}       value
 */
export function getSafeValue(context, keyString) {
    const keyList = keyString.split(".");
    let value = context;
    let key;
    while (value && (keyList.length > 0)) {
        key = keyList.shift();
        value = value[key];
    }
    return value;
}

/**
 * Test whether the event's bubble path includes the given element
 * @param  {Event}    event  dom event
 * @param  {[HtmlElement]} el
 * @return {Boolean}  True if the event's bubble path includes the given element
 */
export function eventPathIncludes(event, el) {
    if (event.path) {
        const path = [].slice.call(event.path);
        return path.includes(el);
    }

    let element = event.target;
    while (element) {
        if (element === el) {
            return true;
        }
        element = element.parentElement;
    }
    return false;
}

/**
 * transform dataURL to blob
 * @param  {String} dataURL
 * @param  {Object} options blob construction options
 * @return {Blob}
 */
export function dataURLToBlob(dataURL, options) {
    // decode base64
    const raw = atob(dataURL.split(",")[1]);
    const len = raw.length;
    const arr = new Uint8Array(len);

    for (let i = 0; i < len; i++) {
        arr[i] = raw.charCodeAt(i);
    }

    return new Blob([ arr ], options);
}

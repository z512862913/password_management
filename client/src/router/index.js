import Vue from "vue";
import VueRouter from "vue-router";

import Promise from "promise";

import * as config from "config";

import App from "container/App";

// install plugin
Vue.use(VueRouter);

// define router
const router = new VueRouter(config.router);

// define router map rules
router.map({
    "/user": {
        component: require("container/User"),
        loginRequired: true,
        subRoutes: {
            "/account": {
                name: "UserAccount",
                component: require("container/UserAccount"),
            },
            "/account/:name": {
                name: "UserAccountDetail",
                component: require("container/UserAccountDetail"),
            },
            "/new": {
                name: "UserNew",
                component: require("container/UserNew"),
            },
            "/password": {
                name: "UserPassword",
                component: require("container/UserPassword"),
            },
        },
    },
    "/auth": {
        component: require("container/Auth"),
        auth: true,
        subRoutes: {
            "/": {
                name: "AuthIndex",
                component: require("container/AuthIndex"),
            },
            "/login": {
                name: "AuthLogin",
                component: require("container/AuthLogin"),
            },
            "/join": {
                name: "AuthJoin",
                component: require("container/AuthJoin"),
            },
            "/reset": {
                name: "AuthReset",
                component: require("container/AuthReset"),
            },
            "/hint/:hint": {
                name: "AuthHint",
                component: require("container/AuthHint"),
            },
        },
    },
});

// define router redirect rules
router.redirect({
    "/user": "/user/account",
    "*": "/auth",
});

router.beforeEach(
    ({ to }) => to.router.app.getLoginStatus().then(() => {
        const { hasLoggedIn } = to.router.app;
        if (!hasLoggedIn) {
            if (!!to.loginRequired) {
                to.router.go({
                    path: "/auth",
                    query: {
                        next: to.path,
                    },
                });
            }
        }
        else if (!!to.auth) {
            to.router.go("/user");
        }

        return Promise.resolve(true);
    })
);

router.start(App, "#app");

export const router = {
    linkActiveClass: "selected",
    suppressTransitionError: true,
};

export const message = {
    error: {
        api: "啊咧，服务器出现故障了>_<",
    },
};

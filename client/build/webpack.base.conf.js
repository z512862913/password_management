var path = require("path");
var config = require("../config");
var utils = require("./utils");
var autoprefixer = require("autoprefixer");
var projectRoot = path.resolve(__dirname, "../");

module.exports = {
    entry: {
        app: "entry",
    },
    output: {
        path: config.dev.assetsRoot,
        publicPath: config.dev.assetsPublicPath,
        filename: utils.assetsPath("js/[name].js"),
        chunkFilename: utils.assetsPath("js/[id].[chunkhash].js"),
    },
    resolve: {
        extensions: [ "", ".js", ".vue" ],
        fallback: [ path.join(__dirname, "../node_modules") ],
        alias: {
            "src": path.resolve(__dirname, "../src"),
            "api": path.resolve(__dirname, "../src/api"),
            "component": path.resolve(__dirname, "../src/component"),
            "container": path.resolve(__dirname, "../src/component/container/"),
            "module": path.resolve(__dirname, "../src/component/module"),
            "config": path.resolve(__dirname, "../src/config"),
            "font": path.resolve(__dirname, "../src/font"),
            "entry": path.resolve(__dirname, "../src/entry"),
            "image": path.resolve(__dirname, "../src/image"),
            "native": path.resolve(__dirname, "../src/native"),
            "plugin": path.resolve(__dirname, "../src/plugin"),
            "resource": path.resolve(__dirname, "../src/resource"),
            "router": path.resolve(__dirname, "../src/router"),
            "store": path.resolve(__dirname, "../src/store"),
            "style": path.resolve(__dirname, "../src/style"),
            "util": path.resolve(__dirname, "../src/util"),
        },
    },
    resolveLoader: {
        fallback: [ path.join(__dirname, "../node_modules") ],
    },
    module: {
        preLoaders: [
            {
                test: /\.vue$/,
                loader: "eslint",
                include: projectRoot,
                exclude: /node_modules/,
            },
            {
                test: /\.js$/,
                loader: "eslint",
                include: projectRoot,
                exclude: /node_modules/,
            },
        ],
        loaders: [
            {
                test: /\.vue$/,
                loader: "vue",
            },
            {
                test: /\.js$/,
                loader: "babel",
                include: projectRoot,
                exclude: /node_modules/,
            },
            {
                test: /\.json$/,
                loader: "json",
            },
            {
                test: /\.html$/,
                loader: "vue-html",
            },
            {
                test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
                loader: "url",
                query: {
                    limit: 10000,
                    name: utils.assetsPath("img/[name].[hash:7].[ext]"),
                },
            },
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
                loader: "url",
                query: {
                    limit: 10000,
                    name: utils.assetsPath("font/[name].[hash:7].[ext]"),
                },
            },
        ],
    },
    eslint: {
        formatter: require("eslint-friendly-formatter"),
    },
    vue: {
        loaders: utils.cssLoaders(),
    },
    postcss: [ autoprefixer({ browsers: [ "last 20 versions" ] }) ],
};

// see http://vuejs-templates.github.io/webpack for documentation.
var path = require("path")

module.exports = {
    build: {
        env: require("./prod.env"),
        index: path.resolve(__dirname, "../../templates/index.html"),
        assetsRoot: path.resolve(__dirname, "../../asset/"),
        assetsSubDirectory: "",
        assetsPublicPath: "/static/",
        productionSourceMap: false,
    },
    dev: {
        env: require("./dev.env"),
        port: 80,
        staticRoot: path.resolve(__dirname, "../../static"),
        staticPublicPath: "/static",
        assetsRoot: "/",
        assetsSubDirectory: "./static/",
        assetsPublicPath: "/",
        proxyList: [
            {
                context: [ "/auth", "/data", "/user", "/static" ],
                options: {
                    target: "http://localhost:2333",
                },
            },
        ],
    },
};

FORMAT: 1A

# 密码锁后台文档
password_management API

## Data Structures
   
### AccountsInCategory

+ name: 社交类 (string, required) - 分类名称

+ accounts: [1,2,3] (array[number], required) - 该分类下账户id的list

# Group 权限
网站权限相关


## 登录 [/auth/login/]

### 用户登录 [POST]

+ Request (application/json)

    + Attributes
    
        + username: admin (string, required) - 用户名
        
        + password: pwd (string, required) - 密码
        
+ Response 200 (application/json)

    + Attributes (object)

        + _status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 用户登录成功 (string, required) - 状态信息
        
+ Response 403 (application/json)

    + Attributes (object)

        + _status (object, required)

            - code: 1 (number, required) - 状态码

            - message: 用户名或密码错误 (string, required) - 状态信息
            
### 获取用户登录状态 [GET]
        
+ Response 200 (application/json)

    + Attributes (object)

        + _status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 成功获取用户登陆状态 (string, required) - 状态信息
        
        + has_logged_in: true (boolean, required) - 是否已经登录
        
        + id: 1 (number, optional) - 登录用户的id
         
        
## 登出 [/auth/logout/]

### 用户登出 [POST]

+ Response 200 (application/json)

    + Attributes (object)

        + _status (object, required)

            - code: 0 (number, required) - 状态码
            
            - message: 用户注销成功 (string, required) - 状态信息


## 注册 [/auth/register/]

### 用户注册 [POST]

+ Request (application/json)

    + Attributes
    
        + username: admin (string, required) - 用户名
        
        + password: pwd (string, required) - 密码
        
+ Response 200 (application/json)

    + Attributes (object)

        + _status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 新用户注册成功 (string, required) - 状态信息
            
        + id: 1 (number, required) - 新用户的id
            
+ Response 400 (application/json)

    + Attributes (object)

        + _status (object, required)

            - code: 4 (number, required) - 状态码

            - message: 该用户名已被注册 (string, required) - 状态信息
            
            
## 修改密码 [/auth/change_password/]

### 用户修改密码 [PUT]

+ Request (application/json)

    + Attributes
    
        + old_password: pwd (string, required) - 原密码
        
        + new_password: new_pwd (string, required) - 新密码
        
+ Response 200 (application/json)

    + Attributes (object)

        + _status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 用户修改密码成功 (string, required) - 状态信息
            
+ Response 403 (application/json)

    + Attributes (object)

        + _status (object, required)

            - code: 1 (number, required) - 状态码

            - message: 原密码错误 (string, required) - 状态信息
          
+ Response 401 (application/json)

    + Attributes (object)

        + _status (object, required)

            - code: 2 (number, required) - 状态码

            - message: 用户未登录 (string, required) - 状态信息
            
     
## 重置密码 [/auth/forget_password/]

### 用户重置密码 [PUT]
     
+ Request (application/json)

    + Attributes
    
        + main_username: username (string, required) - 用户登陆用的用户名
    
        + username1: pwd (string, required) - 用户名1
        
        + password1: pwd (string, required) - 密码1
        
        + username2: pwd (string, required) - 用户名2
        
        + password2: pwd (string, required) - 密码2
        
        + username3: pwd (string, required) - 用户名3
        
        + password3: pwd (string, required) - 密码3
        
        + new_password: pwd (string, required) - 新密码
        
+ Response 200 (application/json)

    + Attributes (object)

        + _status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 重置密码成功，欢迎回来 (string, required) - 状态信息
          
+ Response 403 (application/json)

    + Attributes (object)

        + _status (object, required)

            - code: 4 (number, required) - 状态码

            - message: 认证失败，未能重置密码 (string, required) - 状态信息
            
            
# Group 获取数据
获取相对静态的数据


## 获取所有支持的网站 [/auth/websites/]

### 获取所有支持的网站 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + _status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取所有支持的网站成功 (string, required) - 状态信息
            
        + websites (object, required)
            
            - id: 1 (number, required) - 网站的id
            
            - name: QQ (number, required) - 网站名称
            
            - login_url: http://w.qq.com/ (string, required) - 网站登录url
            
            - icon_path : /static/img/qq.png (string, required) - 图标储存路径
         
            
## 获取所有的账户分类 [/auth/categories/]

### 获取所有的账户分类 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + _status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取所有账户分类成功 (string, required) - 状态信息
            
        + categories (object, required)
            
            - id: 1 (number, required) - 账户分类的id
            
            - name: 社交 (number, required) - 账户分类名称
            
            - icon_path : /static/img/qq.png (string, required) - 图标储存路径
            

# Group 用户操作


## 检查密码强度 [/user/password_strength/]

### 检查用户提交的密码的强度 [PUT]

+ Request (application/json)

    + Attributes
    
        + password: pwd (string, required) - 密码

+ Response 200 (application/json)

    + Attributes (object)

        + _status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取此密码强度成功 (string, required) - 状态信息
            
        + strength (string, required) - 密码强度 强/中/弱
        

## 随机强密码 [/user/random_strong_password/{?allow_uppercase,allow_underscore,allow_special_character}]

### 为用户随机生成一个强密码 [GET]

+ Parameters

    + allow_uppercase: true (boolean, optional) - 密码是否支持大写字母

        + Default: true

    + allow_underscore: true (boolean, optional) - 密码是否支持下划线

        + Default: true

    + allow_special_character: false (boolean, optional) - 密码是否支持特殊字符

        + Default: true

+ Response 200 (application/json)

    + Attributes (object)

        + _status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取随机强密码成功 (string, required) - 状态信息
            
        + password (string, required) - 随机生成的密码
        
        
## 操作用户储存的账户 [/user/operate_account/]

### 为用户储存提交的账户 [POST]

+ Request (application/json)

    + Attributes
    
        + category_id: 1 (number, required) - 账户所属分类的id
        
        + website_id: 1 (number, required) - 账户对应的网站的id
        
        + username: pwd (string, required) - 账户
        
        + password: pwd (string, required) - 密码

+ Response 200 (application/json)

    + Attributes (object)

        + _status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 账户储存成功，我们会好好保管哒 (string, required) - 状态信息
        
        + account (object, required)
        
            - id: 0 (number, required) - 账户id
            
            - website (object, required) - 账户对应的网站信息
            
                - id: 0 (number, required) - 网站的id
                
                - name: QQ (string, required) - 网站的名称
                
                - icon_path: /img/qq.png (string, required) - 图标储存的路径
            
            - username: admin (string, required) - 账户名
            
            - password: admin (string, required) - 密码
            
            - password_strength: 弱 (string, required) - 密码强度
            
            - change_password_time: 2016年8月23日 (string, required) - 密码修改的时间
            
            - category (object, required) - 账户所属的分类信息
            
                - id: 1 (number, required) - 分类的id
                
                - name: 社交类 (string, required) - 分类的名称
                
                - icon_path: /img/social.png (string, required) - 图标储存的路径
            
            - is_overdue: 0 (number, required) - 密码是否陈旧建议修改
            
### 为用户修改储存账户的密码 [PUT]

+ Request (application/json)

    + Attributes
    
        + account_id: 1 (number, required) - 账户的id
        
        + password: pwd (string, required) - 密码

+ Response 200 (application/json)

    + Attributes (object)

        + _status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 修改账户的密码成功 (string, required) - 状态信息
            
### 为用户删除储存的账户 [DELETE]

+ Request (application/json)

    + Attributes
    
        + account_id: 1 (number, required) - 账户的id
        
+ Response 200 (application/json)

    + Attributes (object)

        + _status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 删除账户成功 (string, required) - 状态信息
            
            
## 获取用户常用的电子邮箱 [/user/get_usual_email/]

### 获取用户常用的电子邮箱 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + _status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取常用email成功 (string, required) - 状态信息
            
        + email: zhu@whu.edu.cn (string, required) - 用户常用email
        

## 获取单个账户详情 [/user/get_account_detail/]

### 获取单个账户详情 [GET]

+ Request (application/json)

    + Attributes
    
        + account_id: 1 (number, required) - 账户的id

+ Response 200 (application/json)

    + Attributes (object)

        + _status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取账户详情成功 (string, required) - 状态信息   
              
        + account (object, required)
        
            - id: 0 (number, required) - 账户id
            
            - website (object, required) - 账户对应的网站信息
            
                - id: 0 (number, required) - 网站的id
                
                - name: QQ (string, required) - 网站的名称
                
                - icon_path: /img/qq.png (string, required) - 图标储存的路径
            
            - username: admin (string, required) - 账户名
            
            - password: admin (string, required) - 密码
            
            - password_strength: 弱 (string, required) - 密码强度
            
            - change_password_time: 2016年8月23日 (string, required) - 密码修改的时间
            
            - category (object, required) - 账户所属的分类信息
            
                - id: 1 (number, required) - 分类的id
                
                - name: 社交类 (string, required) - 分类的名称
                
                - icon_path: /img/social.png (string, required) - 图标储存的路径
            
            - is_overdue: 0 (number, required) - 密码是否陈旧建议修改
            
            
## 获取用户储存的所有账户 [/user/get_all_my_account/]

### 获取用户储存的所有账户 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + _status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取用户所有账户成功 (string, required) - 状态信息
            
        + accounts (array[AccountsInCategory], required)


## 获取用户所有密码陈旧建议修改的账户 [/user/get_all_overdue_account/]

### 获取用户所有密码陈旧建议修改的账户 [GET]

+ Response 200 (application/json)

    + Attributes (object)

        + _status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 获取用户所有密码陈旧建议修改的账户成功 (string, required) - 状态信息
            
        + accounts (array[number], required) - 所有建议修改密码的账户id
        
        
            
## 删除数据 [/user/clear_data/]

### 删除所有用户储存的数据 [DELETE]

+ Response 200 (application/json)

    + Attributes (object)

        + _status (object, required)

            - code: 0 (number, required) - 状态码

            - message: 用户全部数据清除成功 (string, required) - 状态信息
